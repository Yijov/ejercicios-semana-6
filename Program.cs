﻿using System;
using System.Text.RegularExpressions;

namespace ejercicios_semana_6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hola");  //
            //PrintTabla();
            //Isprime();
            //numberOfDigits();
            //Aleatory50to100();
            //NumberOfzeros();
            //writes1to50();

            Console.WriteLine("   presione S para salir");
             while (Console.ReadKey().Key != ConsoleKey.S) {
                 Console.WriteLine("  NO es comando reconocido. Por favor,presione S para salir");

             }
        }
            
        static void PrintTabla() //imprime la tabla de multlicar indicada del 1 al 10.
        {
            Console.Write("¿Cual tabla desea ver? ");
            try
            {
                int tabla = Convert.ToInt32(Console.ReadLine());
                int counter = 1;
                while (counter<10)
                {
                    Console.WriteLine( tabla +" X " + counter+"= " + tabla*counter);
                    counter++;
                }
            }
            catch (System.Exception)
            {
                Console.WriteLine("Por favor coloque un número válido");
                PrintTabla();
            }
            
        }

        static void Isprime() //valida si un numero es primo o no.
        {
            Console.Write("Por favor digite un número entero a validar ");
            try
            {
                int numero = Convert.ToInt32(Console.ReadLine());
                bool eval = ComfirmPrime(numero);
                if (eval){
                    Console.WriteLine("El número es primo");
                } else{
                    Console.WriteLine("El número no es primo");
                }
                
            }
            catch (System.Exception)
            {
                Console.WriteLine("Por favor coloque un número válido");
                Isprime();
            }
        }

        static void numberOfDigits() //cuenta los digitos de una cifra
        {
            try
            {
            Console.WriteLine("Por faor digite un número para contar su digitos");
            string input= Convert.ToString(Console.ReadLine());
            int largo= input.Length;
            int numero = Convert.ToInt32(input);
            Console.WriteLine("El numero " + input + " tiene " + largo +" digitos");
                
            }
            catch (System.Exception)
            {
                
                Console.WriteLine("Por favor coloque un número válido");
                numberOfDigits();
            }
            
            
            
        }

        static void Aleatory50to100() //imprime un numero aleatorio entre 50 y 100
        {
            Random randomNumber= new Random();
            Console.WriteLine("su numero aleatorio es "+ randomNumber.Next(50, 101));
            Console.WriteLine("¿Desea tener otro Númeno?    S = si   Otra tecla = No");
            if (Console.ReadKey().Key == ConsoleKey.S){
                Aleatory50to100();
            }
            
        }

        static void NumberOfzeros() //imprime cantidad de ceros desde 1 a la cantidad indicada
        {
            try
            {
                Console.Write("A continuación digite un número, el sistema le dirá cuantos ceros hay desde el número 1 hasta esa cantidad  ");
                int quantity = Convert.ToInt32(Console.ReadLine());
                string acumulatorVal="";
                string search = "0";
                for (int i = 1; i < quantity + 1; i++)
                {
                    acumulatorVal+= Convert.ToString(i);
                }

                
                MatchCollection repeticiones = Regex.Matches(acumulatorVal, search);
                int result= repeticiones.Count;
                Console.WriteLine("Entre 1 y " + quantity + " hay "+ result +" ceros");
                
            }
            catch (System.Exception)
            {
                
                Console.WriteLine("por favor digite un número válido");
                NumberOfzeros();
            }
            
            
        }

        static void writes1to50() //imprime numeros del 1 al 50 con un for loop
        {
            for(int i = 1; i < 51; i++){

                Console.WriteLine(i);

            }
            
            
        }


        static bool ComfirmPrime(int num)//regresa un boolean si el numero es primo o no. este es un método de soporte al método Isprime.
        {

            float cociente= num;
            int residuo = 0;
            bool confirm = true;
            for (int divisor = 2 ; divisor < cociente; divisor++)
            {
                cociente= num/divisor;
                residuo= num % divisor;
                if(residuo==0){
                    confirm = false;
                }
                
            }
            return confirm;
            
            
            
        }
    }
}
